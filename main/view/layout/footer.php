
    <div class="modal fade fixed-right" id=modal-products tabindex=-1 role=dialog aria-hidden=true>
        <div class="modal-dialog modal-vertical" role=document>
            <div class=modal-content>
                <div class="modal-header py-3 align-items-center">
                    <div class=modal-title>
                        <h6 class=mb-0>You might like these</h6>
                    </div><button type=button class=close data-dismiss=modal aria-label=Close><span
                            aria-hidden=true>&times;</span></button>
                </div>
                <div class=modal-body>
                    <p class="text-sm mb-4">Looking for other designs? Check out the other themes we created. Easy to
                        switch, play and develop new websites.</p>
                    <div class="card card-overlay hover-shadow-lg">
                        <div class=h-100><img
                                src=../../../../webpixels.s3.eu-central-1.amazonaws.com/public/themes/purpose-website-ui-kit.png
                                class=card-img-top alt="Purpose Website UI Kit"></div>
                        <div class="card-img-overlay d-flex flex-column align-items-center p-0">
                            <div class="overlay-actions w-100 card-footer mt-auto"><a
                                    href=https://themes.getbootstrap.com/product/purpose-website-ui-kit/
                                    class="h6 mb-0 stretched-link" target=_blank>Purpose Website UI Kit</a></div>
                        </div>
                    </div>
                    <div class="card card-overlay hover-shadow-lg">
                        <div class=h-100><img
                                src=../../../../webpixels.s3.eu-central-1.amazonaws.com/public/themes/purpose-application-ui-kit.png
                                class=card-img-top alt="Purpose Application UI Kit"></div>
                        <div class="card-img-overlay d-flex flex-column align-items-center p-0">
                            <div class="overlay-actions w-100 card-footer mt-auto"><a
                                    href=https://themes.getbootstrap.com/product/purpose-application-ui-kit/
                                    class="h6 mb-0 stretched-link" target=_blank>Purpose Application UI Kit</a></div>
                        </div>
                    </div>
                    <div class="card card-placeholder align-items-center flex-column justify-content-center border-dashed"
                        style=height:250px>
                        <div class="col text-center">
                            <div class=pt-5><i data-feather=smile class=text-primary style=width:50px;height:50px></i>
                                <span class="h5 d-block mt-3">What's next?</span></div>
                            <p class="text-sm px-4 mt-2 mb-0">We are already working on a new product, so stay tuned.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=customizer><button type=button id=btnSwitchMode data-mode=dark
            class="btn btn-primary btn-sm btn-icon-only rounded-circle hover-scale-110 shadow-lg"><i
                class="fas fa-moon"></i></button> <a href=#modal-products data-toggle=modal
            class="btn btn-sm btn-white btn-icon-only rounded-circle hover-scale-110 shadow"><span
                class=btn-inner--icon><i class="fas fa-images"></i></span></a></div>
    <div class="modal fade fixed-right" id=modal-customizer tabindex=-1 role=dialog aria-hidden=true>
        <div class="modal-dialog modal-vertical" role=document>
            <form class=modal-content id=form-customizer>
                <div class=modal-body><button type=button class=close data-dismiss=modal aria-label=Close
                        data-toggle=tooltip data-placement=left title="Close Customizer"><span
                            aria-hidden=true>&times;</span></button>
                    <div class="text-center mx-auto mt-4 mb-5" style=width:80px><img alt="Image placeholder"
                            src=../../assets/img/svg/icons/Click.svg class="svg-inject img-fluid"></div>
                    <h5 class="text-center mb-2">Customize Quick</h5>
                    <p class="text-center mb-4">Customize your preview experience by selecting skin colors and modes.
                    </p>
                    <hr class=mb-4>
                    <h6 class=mb-1>Skin color</h6>
                    <p class="small text-muted mb-3">Set a new theme color palette.</p>
                    <div class="btn-group-toggle row mx-0 mb-5" data-toggle=buttons><label
                            class="btn btn-sm btn-neutral active col mb-2"><input type=radio name=skin value=default
                                checked=checked> Default</label> <label
                            class="btn btn-sm btn-neutral col-6 mb-2 mr-0"><input type=radio name=skin value=blue>
                            Blue</label></div>
                    <h6 class=mb-1>Skin mode</h6>
                    <p class="small text-muted mb-3">Set the theme's mode: light or dark.</p>
                    <div class="btn-group-toggle row mx-0 mb-4" data-toggle=buttons><label
                            class="btn btn-sm btn-neutral active flex-fill mb-2 mr-2"><input type=radio name=mode
                                value=light checked=checked> <i data-feather=sun class=mr-2></i> Light</label> <label
                            class="btn btn-sm btn-neutral flex-fill mb-2 mr-2"><input type=radio name=mode value=dark>
                            <i data-feather=moon class=mr-2></i> Dark</label></div>
                </div>
                <div class="modal-footer border-0"><button type=submit
                        class="btn btn-block btn-primary mt-auto">Preview</button></div>
            </form>
        </div>
    </div>
    <script src=../../assets/libs/jquery/dist/jquery.min.js></script>
    <script src=../../assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js></script>
    <script src=../../assets/libs/svg-injector/dist/svg-injector.min.js></script>
    <script src=../../assets/libs/feather-icons/dist/feather.min.js></script>
    <script src=../../assets/libs/in-view/dist/in-view.min.js></script>
    <script src=../../assets/libs/sticky-kit/dist/sticky-kit.min.js></script>
    <script src=../../assets/libs/imagesloaded/imagesloaded.pkgd.min.js></script>
    <script src=../../assets/libs/apexcharts/dist/apexcharts.min.js></script>
    <script src=../../assets/libs/progressbar.js/dist/progressbar.min.js></script>
    <script src=../../assets/js/quick-website.js></script>
    <script>feather.replace({ width: "1em", height: "1em" })</script>
    <script>!function (e, t, a, n, g) { e[n] = e[n] || [], e[n].push({ "gtm.start": (new Date).getTime(), event: "gtm.js" }); var m = t.getElementsByTagName(a)[0], r = t.createElement(a); r.async = !0, r.src = "../../../../www.googletagmanager.com/gtm4c6a.js?id=GTM-MF4DZVH", m.parentNode.insertBefore(r, m) }(window, document, "script", "dataLayer")</script>
</body>
<!-- Mirrored from preview.webpixels.io/quick-website-ui-kit/pages/boards/overview.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 03 Dec 2020 14:07:27 GMT -->

</html>